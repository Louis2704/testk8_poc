####
FROM maven as builder 
WORKDIR /usr/app
COPY * /usr/app/
RUN mvn clean install
ENTRYPOINT [ "java", "-jar", "/usr/app/target/getting-started-1.0.0-SNAPSHOT-runner.jar" ]

# aws eks update-kubeconfig --region ap-southeast-1 --name EKS-Test-K8s-Cluster